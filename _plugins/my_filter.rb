module Jekyll
  module MyFilters
    def asset_url(input)
      "http://my-cdn.example.com/assets/#{input}"
    end
  end
end

Liquid::Template.register_filter(Jekyll::MyFilters)
